<?php

namespace App\Http\Controllers;

use App\Models\History;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\STR;
use Laravel\Lumen\Routing\Controller as BaseController;
use PhpParser\Node\Stmt\Return_;

class HistoryController extends BaseController
{
    public function get()
    {
        try {
            $res = History::join('users', 'history.user_id', '=', 'users.id')
                ->get(['users.username', 'history.*']);

            return response()->json([
                'status' => true,
                'message' => 'Berhasil mendapatkan data history',
                'data' => $res
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => 'Gagal mendapatkan data history',
                'error' => $th->getMessage()
            ]);
        }
    }
}
