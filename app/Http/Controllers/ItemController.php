<?php

namespace App\Http\Controllers;

use App\Models\History;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\STR;
use Laravel\Lumen\Routing\Controller as BaseController;
use PhpParser\Node\Stmt\Return_;

class ItemController extends BaseController
{
    public function get()
    {
        try {
            return response()->json([
                'status' => true,
                'message' => 'Berhasil mendapatkan data items',
                'data' => Item::all()
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => 'Gagal mendapatkan data items',
                'error' => $th
            ]);
        }
    }

    public function upload(Request $request)
    {
        try {
            // validasi input
            $validation = $this->validate($request, [
                'user_id' => 'required|exists:users,id',
                'username' => 'required|exists:users,username',
                'csv' => 'required|mimes:csv,xlsx|file'
            ]);

            // mengambil file 
            $getFile = $request->file('csv');

            // jika validasi dan mendapatkan file sukses
            if ($validation && $getFile->isValid()) {
                $fileName = STR::random(10) . '.csv';
                $request->file('csv')->move('../storage/uploads', $fileName);
            }

            // menerima input data dari client
            $data = [
                'user_id' => $request->input('user_id'),
                'username' => $request->input('username'),
                'file' => $fileName
            ];

            // menyimpan data history
            History::create($data);

            // membuka file
            $openCSV = fopen("../storage/uploads/" . $fileName, "r");
            $header = true;

            while ($getCsv = fgetcsv($openCSV, 1000, ",")) {
                if ($header) {
                    $header = false;
                } else {
                    Item::create([
                        'nama' => $getCsv[0],
                        'deskripsi' => $getCsv[1],
                        'stok' => $getCsv[2],
                    ]);
                }
            }

            return response()->json(
                [
                    'status' => true,
                    'message' => 'Berhasil upload batch data excel.',
                ],
                200
            );
        } catch (\Throwable $th) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Upload file tidak berhasil disimpan.',
                    'error' => $th->response->original
                ],
                500
            );
        }
    }

    public function download(string $filename)
    {
        $filePath = storage_path("/uploads/{$filename}");

        if (file_exists($filePath)) {
            return response()->download($filePath);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'File not found.'
            ], 404);
        }
    }
}
