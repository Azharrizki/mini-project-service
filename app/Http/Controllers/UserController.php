<?php

namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Routing\Controller as BaseController;

class UserController extends BaseController
{
    public function get()
    {
        try {
            $res = User::all();

            return response()->json([
                'status' => true,
                'message' => 'Berhasil mendapatkan data users',
                'data' => $res
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => 'Gagal mendapatkan data users',
                'error' => $th->getMessage()
            ]);
        }
    }

    public function register(Request $request)
    {
        try {
            $this->validate($request, [
                'username' => 'required|unique:users,username',
                'email' => 'required|email|unique:users,email',
                'password' => 'required'
            ]);

            $password = $request->input('password');

            $encryptPassword = Hash::make($password);

            $data = [
                'username' => $request->input('username'),
                'email' => $request->input('email'),
                'password' => $encryptPassword,
                'created_at' => Date::now(),
                'updated_at' => Date::now()
            ];

            User::create($data);

            return response()->json([
                'status' => true,
                'message' => 'Selamat pendaftaran akun berhasil',
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => 'Pendaftaran akun gagal',
                'error' => $th->getMessage()
            ], 500);
        }
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'email' => 'required',
            'password' => 'required'
        ]);

        $data = [
            'username' => $request->input('username'),
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        ];

        $user = User::where('email', $data['email'])->first();

        if (!Hash::check($data['password'], $user->password)) {
            return response()->json([
                'status' => false,
                'message' => 'Password tidak cocok',
            ]);
        } else {
            return response()->json([
                'status' => true,
                'message' => 'Password benar',
                'password' => $data
            ]);
        }
    }
}
